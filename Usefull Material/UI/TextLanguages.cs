using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextLanguages : MonoBehaviour
{
    [SerializeField] string english;

    string portuguese;

    private void Awake()
    {
        var configurations = FindObjectOfType<Configurations>();
        if (configurations)
            configurations.LanguageUpdate += ChangeLanguage;
    }

    // Start is called before the first frame update
    void Start()
    {
        portuguese = GetComponent<TextMeshProUGUI>().text;

        //Debug.Log(portuguese);
        //Debug.Log(english);

        ChangeLanguage();
    }

    public void ChangeLanguage()
    {
        //Debug.Log("Erase this");
        if (Configurations.gameLanguageIndex == 0)
            GetComponent<TextMeshProUGUI>().text = portuguese;
        else if (Configurations.gameLanguageIndex == 1)
            GetComponent<TextMeshProUGUI>().text = english;
        else
            Debug.LogWarning("The language index " + Configurations.gameLanguageIndex + " is not available");
    }
}
