using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;

public class Configurations : MonoBehaviour
{
    public event System.Action LanguageUpdate;

    [SerializeField] AudioMixer audioMixer;
    [SerializeField] TMP_Dropdown resolutionDropdown;
    [SerializeField] TMP_Dropdown graphicsDropdown;
    [SerializeField] TMP_Dropdown languageDropdown;
    [SerializeField] Slider volumeSlider;
    //[SerializeField] Toggle fullScreen;

    Resolution[] resolutions;

    int currentResolution;
    int currentGraphics;
    public static int gameLanguageIndex;

    // Start is called before the first frame update
    void Start()
    {
        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();
        foreach(Resolution r in resolutions)
        {
            string option = r.width + " x " + r.height;
            options.Add(option);
        }

        resolutionDropdown.AddOptions(options);

        for (int i = 0; i < resolutions.Length; i++)
        {
            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
                currentResolution = i;
        }

        resolutionDropdown.value = currentResolution;
        resolutionDropdown.RefreshShownValue();

        graphicsDropdown.value = QualitySettings.GetQualityLevel();
        graphicsDropdown.RefreshShownValue();

        //SetValues();
        //LanguageUpdate?.Invoke();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetVolume (float volume)
    {
        audioMixer.SetFloat("volume", volume);
        AudioManager.volume = volume;
    }

    public void SetQuality (int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullscreen (bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

    public void SetResolution (int resolutionIndex)
    {
        Screen.SetResolution(resolutions[resolutionIndex].width, resolutions[resolutionIndex].height, Screen.fullScreen);
    }

    public void SetLanguage(int languageIndex)
    {
        gameLanguageIndex = languageIndex;
        LanguageUpdate?.Invoke();
    }

    public void SetValues()
    {
        volumeSlider.value = AudioManager.volume;

        graphicsDropdown.value = QualitySettings.GetQualityLevel();
        graphicsDropdown.RefreshShownValue();

        resolutionDropdown.value = currentResolution;
        resolutionDropdown.RefreshShownValue();

        languageDropdown.value = gameLanguageIndex;
    }
}
