using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{

    [System.Serializable]
    public class Pool
    {
        //Will be how we call the objects
        public string tag;
        public GameObject prefab;
        public int size;
    }

    //Create the list of pools that we'll store in the dictionary
    public List<Pool> pools;

    public Dictionary<string, Queue<GameObject>> poolDictionary;

    void Awake()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        //For each pool stored in our pools list we are going to create a queue of objects

        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            //Here we are going to put the objects in the queue
            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab); //a public atribute of our class "Pool"
                obj.transform.parent = transform;
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            //So then we add the pool, already with the queue of prefabs, in the dictionary
            poolDictionary.Add(pool.tag, objectPool); //we set at the start that our dictionary was going to take a Key (string) and a variable (queue)
        }
    }

    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("Pool with tag: " + tag + " does not exist!");
            return null;
        }

        //To access the queue we use poolDictionary[tag]. Dequeue pull out the first element
        GameObject objectToSpawn = poolDictionary[tag].Dequeue();

        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        var sectionMaster = objectToSpawn.GetComponent<SectionMaster>();
        if (sectionMaster != null)
        {
            sectionMaster.Reset();
            //Debug.Log("Spawned: Section " + sectionMaster.myIndex);
        }


        poolDictionary[tag].Enqueue(objectToSpawn); //put on the queue again
        return objectToSpawn;
    }
}

