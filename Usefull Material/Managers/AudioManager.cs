using UnityEngine.Audio;
using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;


// ReSharper disable Unity.PerformanceCriticalCodeInvocation

public class AudioManager : MonoBehaviour
{
    [SerializeField] Sound[] sounds;

    public static AudioManager instance;

    public float gameSpeed;
    public bool preserveMomentum;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

        foreach (var sound in sounds)
        {
            sound.audioSource = gameObject.AddComponent<AudioSource>();
            sound.audioSource.clip = sound.clip;
            sound.audioSource.volume = sound.volume;
            sound.audioSource.pitch = sound.pitch;
            sound.audioSource.loop = sound.loop;

            Debug.Log("Sounds: " + sound.name);
        }
    }

    private void Start()
    {
        gameSpeed = 1;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            Debug.Log("timescale: " + Time.timeScale);
            Debug.Log("gamespeed: " + gameSpeed);
            Debug.Log("preserve momentum: " + preserveMomentum);
        }
    }

    public void Play(string clipName)
    {
        //Debug.Log("Got here");
        //Debug.Log(clipName);
        var s = Array.Find(sounds, sound => sound.name == clipName);
        if (s != null)
            s.audioSource.Play();
        else
            Debug.LogWarning("Sound: " + clipName + " not found...");

    }

    public void Stop(string clipName)
    {
        var s = Array.Find(sounds, sound => sound.name == clipName);
        if (s != null)
            s.audioSource.Stop();
        else
            Debug.LogWarning("Sound: " + clipName + " not found...");
    }

    public void FadeOut(string clipName, float duration)
    {
        var s = Array.Find(sounds, sound => sound.name == clipName);
        if (s != null)
            StartCoroutine(FadeOutRoutine(s, duration));
        else
            Debug.LogWarning("Sound: " + clipName + " not found...");
    }

    IEnumerator FadeOutRoutine(Sound s, float duration)
    {
        while (s.audioSource.volume > 0)
        {
            s.audioSource.volume += -1 * Time.deltaTime / duration;
            yield return new WaitForEndOfFrame();
        }
    }
}
