using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : Singleton<LevelLoader>
{
    [SerializeField] Canvas loadingScreen;
    //[SerializeField] CanvasGroup canvasGroup;

    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;
        animator = GetComponent<Animator>();
        //loadingScreen.enabled = false;
    }

    public void LoadScene(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator LoadAsynchronously (int sceneIndex)
    {
        animator.SetTrigger("End Scene");
        Debug.Log("before first yield");
        yield return new WaitForSeconds(0.5f);
        Debug.Log("after first yield");
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            //Debug.Log(operation);
            yield return null;
        }

        animator.SetTrigger("Start Scene");

            
    }
}
