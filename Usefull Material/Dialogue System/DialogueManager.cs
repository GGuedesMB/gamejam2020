﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    [SerializeField] bool f2;
    [SerializeField] TextMeshProUGUI nameText;
    [SerializeField] TextMeshProUGUI dialogueText;
    [SerializeField] Text buttonText;
    [SerializeField] Animator dialogueBoxAnimator;
    [SerializeField] float normalTypingSpeed;
    [SerializeField] float startTypingSpeed;

    [SerializeField] TextMeshProUGUI startDialogueText;
    [SerializeField] GameObject startText;
    [SerializeField] GameObject startPanel;

    [SerializeField] GameObject comunicatorIcon;
    [SerializeField] GameObject taylorIcon;
    [SerializeField] GameObject richardIcon;

    public static bool OnDialogue;

    bool useDialogueBox;
    bool jumpTyping;
    bool upgradeOpened;
    //Keep track of all of the sentences
    public Queue<string> sentences;

    Animator myAnimator;

    float typingSpeed;

    bool finishTyping;

    // Start is called before the first frame update
    void Start()
    {
        if (!f2) { startPanel.SetActive(true); }
        sentences = new Queue<string>();
        if (f2) { useDialogueBox = true; }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) && OnDialogue && finishTyping) { DisplayNextSentence(); }
        else if (Input.GetKeyDown(KeyCode.Return) && OnDialogue && !finishTyping) { jumpTyping = true; }
    }

    //funcionamento que pede um "Dialogue"
    public void StartDialogue(Dialogue dialogue)
    {
        OnDialogue = true;
        //comunicatorIcon.SetActive(false);
        richardIcon.SetActive(false);
        taylorIcon.SetActive(false);

        if (useDialogueBox) { dialogueBoxAnimator.SetBool("isOpen", true); nameText.text = dialogue.name; }
        if (nameText.text == "Taylor") { taylorIcon.SetActive(true); }
        else if (nameText.text == "Richard") { richardIcon.SetActive(true); }

        //Clear past sentences;
        sentences.Clear();

        //access all strings
        //obs: sentence é um parametro criado
        foreach (string sentence in dialogue.sentences)
        {
            //colocar uma sentence na fila
            sentences.Enqueue(sentence);
        }
        /*if(sentences.Count == 0) { buttonText.text = "close"; }
        else { buttonText.text = "continue>>"; }*/
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        finishTyping = false;
        jumpTyping = false;
        //se nao tiver mais sentenças na fila, terminar e sair da função;
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
        Debug.Log(sentence);
    }

    IEnumerator TypeSentence(string sentence)
    {
        typingSpeed = normalTypingSpeed;
        //é igual a uma empty string;
        if (useDialogueBox) { dialogueText.text = ""; }else { startDialogueText.text = ""; }

        //para cada char letra(parametro criado) dentro da Lista de letras da sentença 
        //(função transforma string em array de char)
        foreach (char letter in sentence.ToCharArray())
        {
            if (useDialogueBox) { dialogueText.text += letter; } else { startDialogueText.text += letter; }
            //if (!useDialogueBox) { yield return null; }
            if (jumpTyping && useDialogueBox) { dialogueText.text = sentence; finishTyping = true; }
            else if (jumpTyping && !useDialogueBox) { startDialogueText.text = sentence; finishTyping = true; }
            yield return new WaitForSeconds(typingSpeed); //}
        }
        finishTyping = true;
        jumpTyping = false;
    }

    void EndDialogue()
    {
        Debug.Log("End dialogue!");
        dialogueBoxAnimator.SetBool("isOpen", false);
        OnDialogue = false;
        startText.SetActive(false);
        startPanel.SetActive(false);
        useDialogueBox = true;
        finishTyping = false;
        jumpTyping = false;
        if (FindObjectOfType<CutsceneManager>().f2) { FindObjectOfType<CutsceneManager>().f2 = false; CutsceneManager.OnCutscene = false; }
        if (FindObjectOfType<CameraController>().cameraIndex == 7 && !upgradeOpened)
        {
            Invoke("OpenUI", 0.5f);
        }
    }

    private void OpenUI()
    {
        FindObjectOfType<UpgradesUI>().GoToUpgradeUI(); upgradeOpened = true;
    }
}
