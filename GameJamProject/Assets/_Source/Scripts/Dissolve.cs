﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dissolve : MonoBehaviour
{
    [SerializeField] SpriteRenderer sprite;
    [SerializeField] float fadeDuration;

    Material material;

    float fade;
    bool appearing;

    private void Awake()
    {
        material = sprite.material;
    }

    // Update is called once per frame
    void Update()
    {
        appearing = !Input.GetKey(KeyCode.P);

        if (appearing)
            fade += Time.deltaTime / fadeDuration;
        else
            fade -= Time.deltaTime / fadeDuration;

        fade = Mathf.Clamp(fade, 0, 1);

        material.SetFloat("_Fade", fade);
    }

    public void DissolveMaterial(bool appear)
    {
        appearing = appear;
    }

}
