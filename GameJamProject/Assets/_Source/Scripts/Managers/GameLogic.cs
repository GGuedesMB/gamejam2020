﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLogic : MonoBehaviour
{
    [SerializeField] float startSpeed;
    [SerializeField] Animator globalLight;
    [SerializeField] GameObject maryLight;

    public static bool invertedGravity;
    public static float currentSpeed;
    public static bool slowedDown;
    public static bool lightsOff;
    public static float distanceCovered;
    public static int hatchesEggs;

    float pitch;

    // Start is called before the first frame update
    void Start()
    {
        StopAllCoroutines();
        StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        distanceCovered += currentSpeed * Time.deltaTime;
    }

     void StartGame()
    {
        invertedGravity = false;
        currentSpeed = 0;
        slowedDown = false;
        lightsOff = false;
        currentSpeed = startSpeed;
        distanceCovered = 0;
        Physics2D.gravity = Vector2.down * Mathf.Abs(Physics2D.gravity.magnitude);
    }

    public IEnumerator SlowMotion(float slowMotionScale, float slowMotionDuration)
    {
        
        if (!slowedDown)
            pitch = AudioManager.instance.GetPitch("Game Theme");
        AudioManager.instance.ChangePitch("Game Theme", slowMotionScale);
        //Debug.Log("Slow motion duration: " + slowMotionDuration);
        slowedDown = true;
        Time.timeScale = slowMotionScale;
        //Debug.Log("Slowed down: " + Time.timeScale);
        yield return new WaitForSecondsRealtime(slowMotionDuration);
        Time.timeScale = 1;
        slowedDown = false;
        AudioManager.instance.ChangePitch("Game Theme", pitch);
        //Debug.Log("Slow down end: " + Time.timeScale);

    }

    public IEnumerator LightsOff(float lightsOffDuration)
    {
        if (!lightsOff)
            AudioManager.instance.Play("Lights Off");
        lightsOff = true;
        maryLight.SetActive(true);
        globalLight.SetBool("Off", true);
        float timer = 0;
        while (timer < lightsOffDuration)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        globalLight.SetBool("Off", false);
        maryLight.SetActive(false);
        lightsOff = false;
    }

    public IEnumerator GravityInverter(float invertedGravityDuration)
    {
        AudioManager.instance.Play("Inverted Gravity");
        Physics2D.gravity *= -1;
        float timer = 0;
        while (timer < invertedGravityDuration)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        Physics2D.gravity *= -1;
        AudioManager.instance.Play("Normal Gravity");
    }

    public void InvertGravity()
    {
        Physics2D.gravity *= -1;
        if (Physics2D.gravity.y > 0)
            AudioManager.instance.Play("Gravity Inverted");
        else
            AudioManager.instance.Play("Gravity Normal");
    }


}
