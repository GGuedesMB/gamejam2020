﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalInfo : MonoBehaviour
{
    public static GlobalInfo instance;

    public enum Character { Astronaut, Goat, Gnome};
    public Character chosenCharacter;

    public enum Objective { Coins, Time, Enemies };
    public Object chosenObjective;


    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
