using UnityEngine.Audio;
using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

// ReSharper disable Unity.PerformanceCriticalCodeInvocation

public class AudioManager : MonoBehaviour
{
    [SerializeField] Sound[] sounds;

    public static AudioManager instance;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

        foreach (var sound in sounds)
        {
            sound.audioSource = gameObject.AddComponent<AudioSource>();
            sound.audioSource.clip = sound.clip;
            sound.audioSource.volume = sound.volume;
            sound.audioSource.pitch = sound.pitch;
            sound.audioSource.loop = sound.loop;

            Debug.Log("Sounds: " + sound.name);
        }
    }

    public void Play(string clipName)
    {
        //Debug.Log("Got here");
        //Debug.Log(clipName);
        var s = Array.Find(sounds, sound => sound.name == clipName);
        if (s != null)
            s.audioSource.Play();
        else
            Debug.LogWarning("Sound: " + clipName + " not found...");

    }

    public void Stop(string clipName)
    {
        var s = Array.Find(sounds, sound => sound.name == clipName);
        if (s != null)
            s.audioSource.Stop();
        else
            Debug.LogWarning("Sound: " + clipName + " not found...");
    }

    public void FadeOut(string clipName, float duration)
    {
        var s = Array.Find(sounds, sound => sound.name == clipName);
        if (s != null)
            StartCoroutine(FadeOutRoutine(s, duration));
        else
            Debug.LogWarning("Sound: " + clipName + " not found...");
    }

    IEnumerator FadeOutRoutine(Sound s, float duration)
    {
        while (s.audioSource.volume > 0)
        {
            s.audioSource.volume += -1 * Time.deltaTime / duration;
            yield return new WaitForEndOfFrame();
        }
    }

    public void ChangePitch(string clipName, float pitch)
    {
        var s = Array.Find(sounds, sound => sound.name == clipName);
        if (s != null)
            s.audioSource.pitch = pitch;
        else
            Debug.LogWarning("Sound: " + clipName + " not found...");
        Debug.Log(s.pitch);
    }

    public void IncreasePitch(string clipName, float pitch)
    {
        var s = Array.Find(sounds, sound => sound.name == clipName);
        if (s != null)
            s.audioSource.pitch += pitch;
        else
            Debug.LogWarning("Sound: " + clipName + " not found...");
        Debug.Log(s.pitch);
    }

    public float GetPitch(string clipName)
    {
        var s = Array.Find(sounds, sound => sound.name == clipName);
        if (s != null)
            return s.audioSource.pitch;
        else
            return 0;

    }
}
