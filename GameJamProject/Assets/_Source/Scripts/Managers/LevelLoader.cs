using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    public static LevelLoader instance;

    [SerializeField] GameObject loading;
    [SerializeField] Slider loadingSlider;

    Animator animator;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;
        animator = GetComponent<Animator>();
        //loadingScreen.enabled = false;
    }

    public void LoadScene(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator LoadAsynchronously (int sceneIndex)
    {
        animator.SetTrigger("Fade Out");
        Debug.Log("before first yield");
        yield return new WaitForSeconds(0.5f);
        Debug.Log("after first yield");
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        loading.SetActive(true);
        loadingSlider.gameObject.SetActive(true);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            Debug.Log(progress);
            loadingSlider.value = progress;
            yield return null;
        }
        loading.SetActive(false);
        loadingSlider.gameObject.SetActive(false);
        animator.SetTrigger("Fade In");

            
    }
}
