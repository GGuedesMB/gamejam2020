﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SectionMaster : MonoBehaviour
{
    [SerializeField] GameObject firstSection;
    [SerializeField] GameObject[] sections;

    float distanceCovered;
    float endingDistance;

    // Start is called before the first frame update
    void Start()
    {
        FirstSection();
        RandomSection();
    }

    // Update is called once per frame
    void Update()
    {
        distanceCovered += GameLogic.currentSpeed * Time.deltaTime;
        endingDistance -= GameLogic.currentSpeed * Time.deltaTime; 
        if (endingDistance < 8)
            RandomSection();
    }

    void FirstSection()
    {
        GameObject section = Instantiate(firstSection, new Vector2(endingDistance, transform.position.y), Quaternion.identity);
        endingDistance += section.GetComponent<Section>().horizontalLength;
        Debug.Log(endingDistance);
    }

    void RandomSection()
    {
        int random = Random.Range(0, sections.Length);
        GameObject section = Instantiate(sections[random], new Vector2(endingDistance, transform.position.y), Quaternion.identity);
        endingDistance += section.GetComponent<Section>().horizontalLength;
        Debug.Log(endingDistance);
    }
}
