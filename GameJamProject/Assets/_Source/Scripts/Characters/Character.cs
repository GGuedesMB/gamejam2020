﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [Header("Groundcheck")]
    [SerializeField] Transform groundCheck;
    [SerializeField] LayerMask layerMask;
    [SerializeField] float checkRadius = 0.1f;

    Rigidbody2D rb;

    bool ground;

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(groundCheck.position, checkRadius);
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ground = Physics2D.CircleCast(groundCheck.position, checkRadius, Vector2.down, 0, layerMask);
    }

    public void HorizontalMovement(float speed)
    {
        float x = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(x * speed, rb.velocity.y);
    }

    public void AstronautHability(float jumpHigh)
    {
        if (ground)
            rb.velocity = new Vector2(rb.velocity.x, Mathf.Sqrt(jumpHigh * 2 * rb.gravityScale * Physics2D.gravity.magnitude));
    }

    public void GoatHability()
    {
        Debug.Log("Goat hability");
    }
}
