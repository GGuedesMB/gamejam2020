﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astronaut : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float jumpHigh;

    Character character;

    private void Awake()
    {
        character = GetComponent<Character>();
    }
    // Start is called before the first frame update
    void Start()
    {
   
    }

    // Update is called once per frame
    void Update()
    {
        character.HorizontalMovement(speed);
        if (Input.GetButtonDown("Jump")) 
            character.AstronautHability(jumpHigh);
    }
}
