﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggSelector : MonoBehaviour
{
    [Header("Eggs types")]
    [SerializeField] GameObject[] egg;

    [Header("Parameters")]
    public float launchSpeed;
    public float invertedGravityDuration;
    public float lightsOffDuration;
    public float speedMultiplier;
    public float slowMotionScale;
    public float slowMotionDuration;

    [Header("VFX")]
    public ParticleSystem breakVFX;

    public bool broken;
    // Start is called before the first frame update
    void Start()
    {
        EggSelection();
    }

    // Update is called once per frame
    void Update()
    {
        breakVFX.gravityModifier = -Mathf.Sign(Physics2D.gravity.y);
        if (broken)
            breakVFX.transform.position += Vector3.right * -GameLogic.currentSpeed * Time.deltaTime;
    }

    public void EggSelection()
    {
        int random = Random.Range(0, egg.Length);
        egg[random].SetActive(true);
    }
}
