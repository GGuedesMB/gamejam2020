﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinyEggs : MonoBehaviour
{
    public enum Destiny { Gravity, LighsOff, Speed, SlowMotion }
    public Destiny predestinations;

    [SerializeField] EggSelector eggSelector;

    Rigidbody2D rb;
    GameLogic gameLogic;
    SpriteRenderer sp;
    Collider2D col;
    CameraBehaviour cam;
    Signs signs;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        gameLogic = FindObjectOfType<GameLogic>();
        sp = GetComponent<SpriteRenderer>();
        col = GetComponent<Collider2D>();
        cam = FindObjectOfType<CameraBehaviour>();
        signs = FindObjectOfType<Signs>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Launch();
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(1, -Mathf.Sign(Physics2D.gravity.y), 1);
    }

    public void Launch()
    {
        rb.velocity = Vector2.down * eggSelector.launchSpeed * -Mathf.Sign(Physics2D.gravity.y);
        Debug.Log("Egg launched");
    }

    public void BreakEgg()
    {
        /*switch (predestinations)
        {
            case Destiny.Gravity:
                StartCoroutine(gameLogic.GravityInverter(invertedGravityDuration));
                Debug.Log("Gravity");
                break;
            case Destiny.LighsOff:
                StartCoroutine(gameLogic.LightsOff(lightsOffDuration));
                Debug.Log("Lights Off");
                break;
            case Destiny.SlowMotion:
                StartCoroutine(gameLogic.SlowMotion(slowMotionScale, slowMotionDuration));
                Debug.Log("Slow Motion");
                break;
            case Destiny.Speed:
                GameLogic.currentSpeed *= speedMultiplier;
                Debug.Log("Increase Speed");
                break;
            deafult:
                Debug.LogWarning("Egg not founded");
                break;
        }*/

        if (predestinations == Destiny.Gravity)
        {
            gameLogic.InvertGravity();
            signs.ShowSign(0);
        }
            
        else if (predestinations == Destiny.LighsOff)
        {
            StartCoroutine(gameLogic.LightsOff(eggSelector.lightsOffDuration));
            signs.ShowSign(1);
        }
            
        else if (predestinations == Destiny.SlowMotion)
        {
            StartCoroutine(gameLogic.SlowMotion(eggSelector.slowMotionScale, eggSelector.slowMotionDuration));
            signs.ShowSign(2);
        }
            
        else
        {
            GameLogic.currentSpeed *= eggSelector.speedMultiplier;
            AudioManager.instance.IncreasePitch("Game Theme", 0.015f);
            signs.ShowSign(3);
        }
            

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<MaryLu>() == null)
        {
            BreakEgg();
            sp.enabled = false;
            col.enabled = false;
            eggSelector.breakVFX.transform.position = transform.position - Vector3.down * 0.5f * Mathf.Sign(Physics2D.gravity.y);
            eggSelector.breakVFX.Play();
            if (Physics2D.gravity.y > 0)
                eggSelector.breakVFX.transform.Rotate(0,180,0);
            eggSelector.broken = true;
            StartCoroutine(cam.CameraShake(0.2f, 0.4f));
            GameLogic.hatchesEggs++;
            AudioManager.instance.Play("Egg Breaking");
        }

    }
}
