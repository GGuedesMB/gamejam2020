﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaryLu : MonoBehaviour
{
    [Header("Parameters")]
    [SerializeField] float jumpHigh;
    [SerializeField] float secondJumpHigh;
    [SerializeField] float horizontalSpeed;
    [SerializeField] float gravity;
    [SerializeField] float gravityFloating;
    [SerializeField] float speedLost;
    [SerializeField] float speedLostDuration;

    [Header("References")]
    [SerializeField] GameObject eggPrefab;
    [SerializeField] Transform eggSpawn;
    [SerializeField] ParticleSystem runVFX;
    [SerializeField] Animator spriteAnim;
    [SerializeField] GameObject hitFeedback;
    [SerializeField] ParticleSystem feathersVFX;
    [SerializeField] ParticleSystem mudVFX;

    [Header("Groundcheck")]
    [SerializeField] Transform groundCheck;
    [SerializeField] LayerMask layerMask;
    [SerializeField] float checkRadius = 0.1f;

    Rigidbody2D rb;
    Animator anim;
    GameCanvas gameCanvas;

    bool ground;
    bool canDoubleJump;
    bool slowed;
    bool firstInteraction;

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(groundCheck.position, checkRadius);
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        gameCanvas = FindObjectOfType<GameCanvas>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
            feathersVFX.Play();
        if (!slowed)
            HorizontalMovement();

        ground = Physics2D.CircleCast(groundCheck.position, checkRadius, Vector2.down, 0, layerMask);

        if (ground)
            canDoubleJump = true;
        if (Input.GetButtonDown("Jump"))
        {
            if (ground)
                Jump();
            else if (canDoubleJump)
                SecondJump();
        }
        if (Input.GetButton("Jump") && rb.velocity.y * -Mathf.Sign(Physics2D.gravity.y) < 0)
        {
            Float();
            anim.SetBool("Float", true);
        }
        else
        {
            rb.gravityScale = gravity;
            anim.SetBool("Float", false);
        }
            


        if (ground && runVFX.isStopped)
            runVFX.Play();
        else if (!ground && runVFX.isPlaying)
            runVFX.Stop();

        transform.localScale = new Vector2(transform.localScale.x, Mathf.Sign(Physics2D.gravity.y) * -1);

        anim.SetBool("Grounded", ground);
        anim.SetFloat("Vertical Speed", rb.velocity.y * -Mathf.Sign(Physics2D.gravity.y));
        
    }

    public void HorizontalMovement()
    {
        float x = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(x * (GameLogic.currentSpeed + 2), rb.velocity.y);
        rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -GameLogic.currentSpeed - 2, horizontalSpeed), rb.velocity.y);
        if (firstInteraction)
        {
            AudioManager.instance.Play("Start");
            firstInteraction = false;
        }
    }

    public void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, Mathf.Sqrt(jumpHigh * 2 * rb.gravityScale * Physics2D.gravity.magnitude) * -Mathf.Sign(Physics2D.gravity.y));
        anim.SetTrigger("Jump");
        AudioManager.instance.Play("Jump");
        if (firstInteraction)
        {
            AudioManager.instance.Play("Start");
            firstInteraction = false;
        }
    }

    public void SecondJump()
    {
        rb.velocity = new Vector2(rb.velocity.x, Mathf.Sqrt(secondJumpHigh * 2 * rb.gravityScale * Physics2D.gravity.magnitude) * -Mathf.Sign(Physics2D.gravity.y));
        Hatch();
        canDoubleJump = false;
        anim.SetTrigger("Double Jump");
        AudioManager.instance.Play("Double Jump");
        feathersVFX.Play();
    }

    public void Float()
    {
        rb.gravityScale = gravityFloating; 
    }

    public void Hatch()
    {
        GameObject egg = Instantiate(eggPrefab, eggSpawn.position, Quaternion.identity);
        //egg.GetComponent<EggSelector>().EggSelection();
    }

    IEnumerator Hit()
    {
        feathersVFX.Play();
        hitFeedback.SetActive(true);
        AudioManager.instance.Play("Hit");
        Debug.Log("Hit");
        anim.SetTrigger("Hit");
        spriteAnim.SetBool("Invulnerable", true);
        slowed = true;
        float timer = 0;
        float delay = speedLost;
        while (timer < speedLostDuration)
        {
            rb.velocity = new Vector2(delay, rb.velocity.y);
            //delay *= 0.98f;
            timer += Time.deltaTime;
            yield return null;
        }
        slowed = false;
        spriteAnim.SetBool("Invulnerable", false);
        Debug.Log("Hit end");
        hitFeedback.SetActive(false);
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Slower"))
            StartCoroutine(Hit());
        if (collision.gameObject.CompareTag("Death"))
        {
            AudioManager.instance.Play("End");
            GameLogic.currentSpeed = 0;
            gameCanvas.GameOver();
        }
        if (collision.GetComponent<Mud>() != null)
            mudVFX.Play();
    }
}
