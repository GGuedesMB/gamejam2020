﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Section : MonoBehaviour
{
    [SerializeField] float speed;

    public float horizontalLength;

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.right * -GameLogic.currentSpeed * Time.deltaTime;
    }
}
