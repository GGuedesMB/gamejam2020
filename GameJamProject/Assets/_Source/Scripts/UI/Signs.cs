﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Signs : MonoBehaviour
{
    [SerializeField] GameObject[] signs;

    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowSign(int index)
    {
        for (int i = 0; i < signs.Length; i++)
        {
            if (i == index)
                signs[i].SetActive(true);
            else
                signs[i].SetActive(false);
        }
        anim.SetTrigger("Sign");
    }
}
