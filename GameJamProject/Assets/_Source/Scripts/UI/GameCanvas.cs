﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameCanvas : MonoBehaviour
{
    [SerializeField] AudioClip mainButtonSound;
    [SerializeField] AudioClip[] buttonSounds;
    [SerializeField] TextMeshProUGUI meters;
    [SerializeField] TextMeshProUGUI eggs;

    Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void Start()
    {
        AudioManager.instance.Play("Game Theme");
    }

    public void GameOver()
    {
        AudioManager.instance.FadeOut("Game Theme", 0.3f);
        AudioManager.instance.Play("Game Over Theme");
        anim.SetTrigger("Game Over");
        int distance = (int) GameLogic.distanceCovered;
        meters.text = distance.ToString() + " m";
        eggs.text = "x " + GameLogic.hatchesEggs.ToString();
    }

    public void Menu()
    {
        AudioManager.instance.FadeOut("Game Over Theme", 0.3f);
        LevelLoader.instance.LoadScene(0);
    }
    public void Retry()
    {
        AudioManager.instance.FadeOut("Game Over Theme", 0.3f);
        LevelLoader.instance.LoadScene(1);
    }

    public void ButtonSound()
    {
        AudioSource.PlayClipAtPoint(buttonSounds[Random.Range(0, buttonSounds.Length)], Camera.main.transform.position, 0.5f);
    }

    public void ButtonMainSound()
    {
        AudioSource.PlayClipAtPoint(mainButtonSound, Camera.main.transform.position, 0.5f);
    }
}
