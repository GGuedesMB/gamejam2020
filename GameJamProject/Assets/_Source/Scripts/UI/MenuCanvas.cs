﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCanvas : MonoBehaviour
{
    [SerializeField] AudioClip mainButtonSound;
    [SerializeField] AudioClip[] buttonSounds;
    Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void Start()
    {
        AudioManager.instance.Play("Menu Theme");
    }

    //Animation functions
    public void ShowMenu() { anim.SetTrigger("Show Menu"); }
    public void ShowOptions() { anim.SetTrigger("Show Options"); }
    public void ShowCredits() { anim.SetTrigger("Show Credits"); }
    public void Back() { anim.SetTrigger("Back"); }

    public void Play()
    {
        AudioManager.instance.FadeOut("Menu Theme", 1);
        LevelLoader.instance.LoadScene(1);
    }
    public void Quit() { Application.Quit(); }

    public void ButtonSound()
    {
        AudioSource.PlayClipAtPoint(buttonSounds[Random.Range(0, buttonSounds.Length)], Camera.main.transform.position, 0.5f);
    }

    public void ButtonMainSound()
    {
        AudioSource.PlayClipAtPoint(mainButtonSound, Camera.main.transform.position, 0.5f);
    }
}
