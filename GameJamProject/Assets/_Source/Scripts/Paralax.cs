﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paralax : MonoBehaviour
{
    [SerializeField] Transform[] sprites;
    [SerializeField] float horizontalLength;
    [SerializeField] float multiplier;

    float distanceCovered;
    int index;
    
    // Start is called before the first frame update
    void Start()
    {
        index = sprites.Length * 2;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 delta = Vector3.right * -GameLogic.currentSpeed * Time.deltaTime * multiplier;
        foreach (Transform sprite in sprites)
            sprite.position += delta;

        distanceCovered += delta.magnitude;
        if (distanceCovered >= horizontalLength)
        {
            int currentIndex = index % sprites.Length;
            sprites[currentIndex].position = sprites[(index - 1) % sprites.Length].position + Vector3.right * horizontalLength;
            index++;
            distanceCovered = 0;
        }

    }
}
